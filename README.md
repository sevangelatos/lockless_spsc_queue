# lockless_spsc_queue

This is a sample implementation of a lockless ring-buffer based Single Producer Single Consumer queue. It is meant to be simple and efficient.
The implementation (queue_spsc.h) is based on std::atomic<int> and a preallocated std::vector for the underlying buffer.
An equivalent implementation with a mutex (queue_mutex.h) is also provided.
To compile a simple test application that is meant to showcase the queues, use either LocklessSPSCQueue.sln (Visual Studio 2013) or 
use the provided Makefile for unix/linux.


