TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpthread

SOURCES += \
    LocklessSPSCQueue/LocklessSPSCQueue.cpp

HEADERS += \
    LocklessSPSCQueue/queue_spsc.h \
    LocklessSPSCQueue/queue_mutex.h
